<?php include 'conexionp.php';
$id_asignatura = $_REQUEST['id_asignatura'];
$id_libro = $_REQUEST['id_libro'];
$cedula = $_REQUEST['cedula'];


$sel = $con -> query("SELECT * FROM libros where id_asignatura=$id_asignatura and id_libro=$id_libro");
if ($fila = $sel -> fetch_assoc()) {
}
 ?>
<!DOCTYPE html>
<html>
<head>
	<title>ACTUALIZAR LIBRO</title>
	<meta name="viewport" content="initial-scale=1.0">
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <style>
      .container{margin-top:100px}
    </style>
</head>
<body>

<center><h3><b>EDITAR BIBLIOGRAFIA </b></h3></center>
<center>
<br><br>	

<form action="updatep.php" method="post">
	<input type="hidden" name="id_asignatura" value="<?php echo $id_asignatura?>">
	<input type="hidden" name="cedula" value="<?php echo $cedula?>">
	<input type="hidden" name="id_libro" value="<?php echo $id_libro?>">
    
    <div class="form-group"> <!-- Full Name -->
        <label for="full_name_id" class="control-label">Nombre del Autor</label>
        <input type="text" class="form-control" name="autor" placeholder="Nombre del Autor" value="<?php echo $fila['autor']?>">
    </div>    

	<div class="form-group"> <!-- Full Name -->
        <label for="full_name_id" class="control-label">Titulo del Libro</label>
        <input type="text" class="form-control" name="titulo" placeholder="Titulo del Libro" value="<?php echo $fila['titulo'] ?>" required>
    </div>

    <div class="form-group"> <!-- Full Name -->
        <label for="full_name_id" class="control-label">Año</label>
        <input type="text" class="form-control" name="anio" placeholder="Año" value="<?php echo $fila['anio'] ?>" onkeypress="solonumeros(event);">
    </div>

    <div class="form-group"> <!-- Full Name -->
        <label for="full_name_id" class="control-label">Ciudad</label>
        <input type="text" class="form-control" name="ciudad" placeholder="Ciudad" value="<?php echo $fila['ciudad'] ?>">
    </div>

    <div class="form-group"> <!-- Full Name -->
        <label for="full_name_id" class="control-label">Editorial</label>
        <input type="text" class="form-control" name="editorial" placeholder="Editorial" value="<?php echo $fila['editorial'] ?>">
    </div>
    
    <div class="form-group"> <!-- Full Name -->
        <label for="full_name_id" class="control-label">Edicion</label>
        <input type="text" class="form-control" name="edicion" placeholder="Edicion" value="<?php echo $fila['edicion'] ?>">
    </div>    

	<div class="form-group"> <!-- State Button -->
		<label for="full_name_id" class="control-label">Estado</label>
        <select name="estado" class="form-control" id="estado" required>
            <option></option>
            <option>Adquirido</option>
            <option>En Proceso de Adquisición</option>
            <option>No se ha planificado su Adquisición</option>
        </select>
    </div>

    <div class="form-group"> <!-- Submit Button -->
        <button type="submit" class="btn btn-primary">Guardar</button>
    </div>     
</form>

<script>
function solonumeros(e)
                    {
         var key = window.event ? e.which : e.keyCode;
                        if(key < 48 || key > 57)
                            e.preventDefault();
                    }
</script>
</center>
<br>
<br>


</body>
</html>