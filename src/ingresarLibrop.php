<?php include 'conexionp.php';
	$id_asignatura = $_REQUEST['id_asignatura'];
	$cedula = $_REQUEST['cedula'];
	$asignatura = $_REQUEST['asignatura'];
	/*echo "id_asignatura Pepe: ", $id_asignatura;
	echo "<br>";
	echo "cedula Pepe: ", $cedula;
	echo "<br>";
	echo "Asignatura: ", $asignatura;*/
?>

<!DOCTYPE html>
<html>
<head>
	<title>INGRESAR EL LIBRO</title>
	<meta name="viewport" content="initial-scale=1.0">
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <style>
      .container{margin-top:100px}
    </style>
</head>
<body>
<br><br>
<center><h3><b>INGRESAR EL LIBRO PARA LA MATERIA DE: <?php echo "$asignatura" ?></b></h3></center>
<center>
<br><br>	
<form action="guardarp.php" method="POST">
	<input type="hidden" name="id_asignatura" value="<?php echo $id_asignatura?>">
	<input type="hidden" name="cedula" value="<?php echo $cedula?>">
    <input type="hidden" name="asignatura" value="<?php echo $asignatura?>">
    
    <div class="form-group"> <!-- Full Name -->
        <label for="full_name_id" class="control-label">Nombre del Autor</label>
        <input type="text" class="form-control" name="autor" placeholder="Nombre del Autor">
    </div>    

	<div class="form-group"> <!-- Full Name -->
        <label for="full_name_id" class="control-label">Titulo del Libro</label>
        <input type="text" class="form-control" name="titulo" placeholder="Titulo del Libro">
    </div>

    <div class="form-group"> <!-- Full Name -->
        <label for="full_name_id" class="control-label">Año</label>
        <input type="text" class="form-control" name="anio" placeholder="Año" onkeypress="solonumeros(event);">
    </div>

    <div class="form-group"> <!-- Full Name -->
        <label for="full_name_id" class="control-label">Editorial</label>
        <input type="text" class="form-control" name="editorial" placeholder="Editorial">
    </div>
    
    <div class="form-group"> <!-- Full Name -->
        <label for="full_name_id" class="control-label">Edicion</label>
        <input type="text" class="form-control" name="edicion" placeholder="Edicion">
    </div>    

	<div class="form-group"> <!-- State Button -->
        <label for="estado" class="control-label">Estado</label>
        <select name="estado" class="form-control" id="state_id">
            <option></option>
            <option>Adquirido</option>
			<option>En Proceso de Adquisicion</option>
			<option>No se ha planificado su Adquisicion</option>
        </select>         
    </div>

    <div class="form-group"> <!-- Full Name -->
        <label for="full_name_id" class="control-label">Ciudad</label>
        <input type="text" class="form-control" name="ciudad" placeholder="Ciudad">
    </div>
      
    <div class="form-group"> <!-- Submit Button -->
        <button type="submit" class="btn btn-primary">Guardar</button>

        <a href="formulariop2.php?&cedula=<?php echo $cedula?>">CANCELAR</a></center>
    </div>     
</form>

<script>
function solonumeros(e)
                    {
         var key = window.event ? e.which : e.keyCode;
                        if(key < 48 || key > 57)
                            e.preventDefault();
                    }
</script>
</center>
<br>
<br>

</body>
</html>
