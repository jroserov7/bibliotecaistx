El presete proyecto permite al docente del Instituto Superior Tecnológico Cotopaxi, realizar el ingreso de la bibliografia para cada una de las materias que imparte en la Institución.
El proyecto será versionado en Git como insfraestrura como codigo en dos versiones de php, que es la php 5 y la php7.

Para probar el proyecto por favor siga los siguientes pasos:

1) En la parte superior derecha del proyecto, dar clic en clonar proyecto, seleccionar copiar.
2) Abrir una terminal linux. El linux debe tener instalado el docker, y el docker-compose previamente. Si no lo tiene instalado ejecutar en la terminal los comandos para instalar el docker y el docker-composeÑ
   sudo apt install docker
   sudo apt install docker-compose
3) Digitar sudo su para trabajar como administrador. La clave será solicitada por unica vez antes de trabajar en modo administrador
4) Digitar git clone (direccion http:// que copio del proyecto en el paso 1)
5) Digitar ls
6) Ingresar al dierectorio bibliotecaistx, digitando cd bibliotecaistx
7) Digitar ls
8) Observar que la misma estructura que consta en el git, consta dentro de la carpeta biliotecaistx.
9) Para correr el proyecto en la version php7, siga los siguientes pasos
  9.1) docker-compose build –no-cache
  9.2) docker-compose up
  9.3) Abra el navegador de su preferencia, y en el url digitar 192.168.100.250:8097/login.php
  9.4) Para dejar de usar el contenedor, en la terminal digitar docker-compose down
11) Para revisar la infraestructura como codigo de como estuvo configurado con php5, haga referencia al tag V1php5
12) Para ejecutar el sistema de biblioteca en php 5 ejecutar
  12.1) docker-compose up
  12.2) Abra el navegador de su preferencia, y en el url digitar 192.168.100.250:8090/login.php
  12.3) Para dejar de usar el contenedor, en la terminal digitar docker-compose down
